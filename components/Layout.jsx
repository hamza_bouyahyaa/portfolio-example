import React, { useState } from "react";
import styles from "../styles/Layout.module.css";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import Person2OutlinedIcon from "@mui/icons-material/Person2Outlined";
import IntegrationInstructionsOutlinedIcon from "@mui/icons-material/IntegrationInstructionsOutlined";
import HomeRepairServiceOutlinedIcon from "@mui/icons-material/HomeRepairServiceOutlined";
import LocalPhoneOutlinedIcon from "@mui/icons-material/LocalPhoneOutlined";
import Link from "next/link";
const Layout = (props) => {
  const [isExpanded, setExpendState] = useState(true);
  const menuItems = [
    {
      text: "Home",
      icon: <HomeOutlinedIcon />,
      path: "home",
    },
    {
      text: "About",
      icon: <Person2OutlinedIcon />,
      path: "about",
    },
    {
      text: "Skills",
      icon: <IntegrationInstructionsOutlinedIcon />,
      path: "skills",
    },
    {
      text: "Works",
      icon: <HomeRepairServiceOutlinedIcon />,
      path: "works",
    },
    {
      text: "Contact",
      icon: <LocalPhoneOutlinedIcon />,
      path: "contact",
    },
  ];
  return (
    <div style={{ display: "flex", minHeight: "100%" }}>
      <div
        className={
          isExpanded
            ? styles.side_nav_container
            : `${styles.side_nav_container} ${styles.side_nav_container_NX}`
        }
      >
        <div className={styles.nav_upper}>
          <div className={styles.nav_heading}>
            {isExpanded && (
              <h2
                style={{
                  fontFamily: "'Poppins', sans-serif !important",
                  fontSize: "32px",
                }}
              >
                Mi
              </h2>
            )}
          </div>

          <div className={styles.nav_menu}>
            {menuItems.map(({ text, icon, path }, index) => (
              <Link
                className={
                  isExpanded
                    ? styles.menu_item
                    : `${styles.menu_item} ${styles.menu_item_NX}`
                }
                href={`#${path}`}
                scroll
                key={index}
              >
                <span style={{ padding: "0 20px" }}>{icon}</span>

                {isExpanded && (
                  <p
                    style={{
                      fontFamily: "'Poppins', sans-serif !important",
                      fontSize: "14px",
                      fontWeight: 400,
                    }}
                  >
                    {text}
                  </p>
                )}
              </Link>
            ))}
          </div>
        </div>
      </div>
      <div style={{ flexGrow: 1, minHeight: "100%", background: "#F7F7F7" }}>
        {props.children}
      </div>
    </div>
  );
};
export default Layout;
