import React from "react";
import Avatar from "@mui/material/Avatar";
import styles from "../styles/Layout.module.css";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import GitHubIcon from "@mui/icons-material/GitHub";
import TwitterIcon from "@mui/icons-material/Twitter";

export default function Accueil() {
  return (
    <div
      id="home"
      style={{
        maxWidth: "868px",
        margin: "150px auto",
        display: "flex",
        gap: "70px",
      }}
    >
      <Avatar
        alt="Mohamed Idoudi"
        src="https://media.discordapp.net/attachments/989223505147072585/1070470328918212608/idoudi.jpeg"
        sx={{ width: 350, height: 350 }}
      />
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          gap: "25px",
          paddingTop: "50px",
        }}
      >
        <div>
          <span
            style={{
              fontFamily: "'Poppins', sans-serif !important",
              fontSize: "16px",
              fontWeight: 400,
              color: "#333333",
              lineHeight: "19px",
            }}
          >
            HI THERE! I&apos;M{" "}
          </span>
        </div>
        <div>
          <span
            style={{
              fontFamily: "'Poppins', sans-serif !important",
              fontSize: "40px",
              fontWeight: 400,
              color: "#333333",
            }}
          >
            <span
              style={{
                color: "#8444df",
              }}
            >
              Mohamed{" "}
            </span>
            Idoudi
          </span>
        </div>
        <div>
          <span
            style={{
              fontFamily: "'Poppins', sans-serif !important",
              fontSize: "20px",
              fontWeight: 500,
              color: "#5f6f81",
              lineHeight: "25px !important",
            }}
          >
            A{" "}
            <span
              style={{
                color: "#8444df",
              }}
            >
              Front-End Web Developer{" "}
            </span>{" "}
            passionate about creating interactive applications and experiences
            on the web.
          </span>
        </div>
        <div style={{ display: "flex", gap: "50px", alignItems: "center" }}>
          <button className={styles.btn}>Resumé</button>
          <div style={{ display: "flex", gap: "25px" }}>
            <div
              style={{
                padding: "5px",
                background: "white",
                borderRadius: "9px",
              }}
            >
              <LinkedInIcon
                style={{
                  color: "#8444df",
                }}
              />
            </div>
            <div
              style={{
                padding: "5px",
                background: "white",
                borderRadius: "9px",
              }}
            >
              <GitHubIcon
                style={{
                  color: "#8444df",
                }}
              />
            </div>
            <div
              style={{
                padding: "5px",
                background: "white",
                borderRadius: "9px",
              }}
            >
              <TwitterIcon
                style={{
                  color: "#8444df",
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
