import React from "react";
import JavascriptIcon from "@mui/icons-material/Javascript";

const Skills = () => {
  const skills = [
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
    { text: "Javascript", icon: <JavascriptIcon /> },
  ];
  return (
    <div
      id="skills"
      style={{ maxWidth: "868px", margin: "0 auto", paddingTop: "120px" }}
    >
      <div
        style={{
          display: "flex",
          maxWidth: "20%",
          alignItems: "center",
          paddingBottom: "20px",
        }}
      >
        <hr style={{ width: "35%", margin: "0 auto" }} />
        <span
          style={{
            fontFamily: "'Poppins', sans-serif !important",
            fontSize: "13px",
            fontWeight: 400,
            color: "#333333",
            lineHeight: "10px",
            textTransform: "capitalize",
          }}
        >
          Some info
        </span>
      </div>
      <div>
        <span
          style={{
            fontFamily: "'Poppins', sans-serif !important",
            fontSize: "26px",
            fontWeight: 500,
            color: "#333333",
            lineHeight: "26px",
            textTransform: "capitalize",
            paddingLeft: "22px",
          }}
        >
          SKILLS
        </span>
      </div>
      <div
        style={{
          padding: "15px",
          background: "white",
          borderRadius: "30px",
          marginTop: "35px",
          display: "flex",
          flexWrap: "wrap",
          gap: "50px",
          marginBottom: "100px",
        }}
      >
        {skills.map((skill, index) => (
          <div
            key={index}
            style={{
              display: "flex",
              flexDirection: "column",
              boxShadow: "0px 4px 8px rgb(134 151 168 / 10%)",
              padding: "10px",
              border: "1px solid #eee",
              borderRadius: "15px",
              maxWidth: "80px",
            }}
          >
            {skill.icon}
            <span
              style={{
                textAlign: "center",
                fontFamily: "'Poppins', sans-serif !important",
                fontSize: "11px",
                fontWeight: 500,
                color: "#333333",
                lineHeight: "11px",
              }}
            >
              {skill.text}
            </span>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Skills;
