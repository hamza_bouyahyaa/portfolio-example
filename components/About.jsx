import React from "react";

const About = () => {
  return (
    <div
      id="about"
      style={{ maxWidth: "868px", margin: "0 auto", paddingTop: "120px" }}
    >
      <div
        style={{
          display: "flex",
          maxWidth: "20%",
          alignItems: "center",
          paddingBottom: "20px",
        }}
      >
        <hr style={{ width: "35%", margin: "0 auto" }} />
        <span
          style={{
            fontFamily: "'Poppins', sans-serif !important",
            fontSize: "13px",
            fontWeight: 400,
            color: "#333333",
            lineHeight: "10px",
            textTransform: "capitalize",
          }}
        >
          Some info
        </span>
      </div>
      <div>
        <span
          style={{
            fontFamily: "'Poppins', sans-serif !important",
            fontSize: "26px",
            fontWeight: 500,
            color: "#333333",
            lineHeight: "26px",
            textTransform: "capitalize",
            paddingLeft: "22px",
          }}
        >
          ABOUT ME
        </span>
      </div>
      <div
        style={{
          padding: "15px",
          background: "white",
          borderRadius: "30px",
          marginTop: "35px",
        }}
      >
        <p
          style={{
            fontFamily: "'Poppins', sans-serif !important",
            fontSize: "20px",
            fontWeight: 500,
            color: "#333333",
            lineHeight: "30px",
          }}
        >
          I&apos;m a front-end web developer with a background in computer systems
          and network infrastructure. My 8 years of IT experience has given me a
          strong foundation for web development and building complex solutions.
          Recently, I graduated from the{" "}
          <span
            style={{
              color: "#8444df",
            }}
          >
            Juno College Immersive Web Development Bootcamp.
          </span>{" "}
          I am passionate about coding and solving problems through code, and I
          am excited to work alongside other amazing programmers and learn so
          much more!
        </p>
      </div>
    </div>
  );
};

export default About;
