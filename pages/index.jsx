import About from "../components/About";
import Accueil from "../components/Accueil";
import Skills from "../components/Skills";

export default function Home() {
  return (
    <>
      {" "}
      <Accueil />
      <About />
      <Skills />
    </>
  );
}
